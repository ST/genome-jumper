<?php
  $file = "genome-jumper-mails.txt"; 
  if (isset($_POST['EMAIL'])) { // check if both fields are set
    $email=$_POST['EMAIL']; 
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      file_put_contents($file, $email."\n", FILE_APPEND);
      http_response_code(200);
    }else {
      http_response_code(400);
    }
  }else {
    http_response_code(403);
  }
?>
